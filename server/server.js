const express = require('express');

const app = express();
const server = require('http').Server(app);

server.listen(3333);

app.use(express.static('public'));

module.exports = {
  app,
  server,
};
