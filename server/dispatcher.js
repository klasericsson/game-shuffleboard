const { response } = require('../common/response');

const { server } = require('./server');
const shortid = require('shortid');
const debug = require('debug')('dispatcher');
const debugMessage = require('debug')('message');
const io = require('socket.io')(server);
// how long we wait for a response until we reject it
const responseTimeout = 3000;
// how often we should check for inactive clients
const cleanupInterval = 3600000;
// time in ms before inactive clients get removed
const cleanupThreshold = 3600000;

const Dispatcher = () => {
  const allClients = new Map();
  const getClientById = (clientId) => allClients.get(clientId);
  const setClient = (clientId, object) => allClients.set(clientId, object);

  const cleanup = () => {
    for (const client of allClients.values()) {
      if (!client.socket || (client.socket.disconnected && Date.now() - client.lastActive > cleanupThreshold)) {
        allClients.delete(client.clientId);
      }
    }
  };
  setInterval(cleanup, cleanupInterval);

  const renameClient = ({ oldId, newId, socketId }) => new Promise((resolve, reject) => {
    debug(`Renaming client ${oldId} to ${newId}`);
    const client = getClientById(oldId);
    const existingClientWithNewId = getClientById(newId);
    const existingClientIsActive = existingClientWithNewId && existingClientWithNewId.socket && existingClientWithNewId.socket.connected;
    if (!client) {
      return reject(`Could not rename client. ClientId (${oldId}) doesn't exist.`);
    }
    if (existingClientIsActive && existingClientWithNewId.socket.id !== `/#${socketId}`) {
      return reject(`Could not rename client. An active client with the same id (${newId}) already exists.`);
    }
    client.clientId = newId;
    allClients.delete(oldId);
    setClient(newId, client);
    return resolve(client);
  });

  const sendRequestToClient = payload => new Promise((resolve, reject) => {
    if (!payload.recipient) reject('Missing parameter: recipient');
    const receivingClient = getClientById(payload.recipient);
    if (receivingClient) {
      let receivedResponse = false;
      const senderSocketId = getClientById(payload.clientId).socket.id;
      receivingClient.socket.emit(payload.eventName, Object.assign(payload, { socketId: senderSocketId }), (payloadFromRecipient) => {
        receivedResponse = true;
        resolve(payloadFromRecipient);
      });
      setTimeout(() => {
        if (!receivedResponse) {
          reject(`Response from recipient (${payload.recipient}) timed out.
          Is recipient listening to ${payload.eventName} events?`);
        }
      }, responseTimeout);
    } else {
      reject(`Recipient with id "${payload.recipient}" does not exist`);
    }
  });

  const broadcast = payload => {
    console.log('BROADCASTING!!');
    console.log(payload);
    const client = getClientById(payload.clientId);
    if (client && client.socket) {
      client.socket.broadcast.emit(payload.eventName, payload);
    }
  };

  const roomBroadcast = payload => {
    const client = getClientById(payload.clientId);
    if (client && client.socket) {
      client.socket.broadcast.to(payload.roomId).emit(payload.eventName, payload);
    }
  };

  const handlers = {
    CLIENT_REQUEST: (payload, callback) => sendRequestToClient(payload)
      .then(responseFromMonitor => callback(responseFromMonitor))
      .catch(error => callback(response.error(error))),

    BROADCAST: broadcast,

    ROOM_BROADCAST: roomBroadcast,

    JOIN_MONITOR_REQUEST: (payload, callback) =>
      sendRequestToClient(payload)
      .then(responseFromMonitor => {
        const controllerClient = getClientById(payload.clientId);
        if (controllerClient && controllerClient.socket) {
          controllerClient.monitorClientId = payload.recipient;
          controllerClient.socket.join(payload.recipient);
        }
        callback(responseFromMonitor);
      })
      .catch(errorFromRecipient => callback(
        response.error(`JOIN_MONITOR_REQUEST: ${errorFromRecipient}`))
      ),

    RENAME_CLIENT: (payload, callback) => renameClient(payload)
      .then(client => {
        if (payload.monitorClientId) {
          client.socket.join(payload.monitorClientId);
        }
        callback(response.ok(client.clientId));
      })
      .catch((error) => callback(response.error(`RENAME_CLIENT: ${error}`))),
  };

  const clientConnected = (socket) => {
    const clientId = shortid.generate();
    const client = {
      clientId,
      socket,
      lastActive: Date.now(),
    };
    allClients.set(clientId, client);
    debug(`Client connected. Assigning clientId ${clientId}`);
    debug(`Total clients: ${allClients.size}`);

    socket.on('disconnect', () => {
      debug(`client (${clientId}) disconnected`);
      client.lastActive = Date.now();
      debug(`Clients left: ${allClients.size}`);
      if (client.monitorClientId) {
        const monitor = getClientById(client.monitorClientId);
        socket.broadcast.to(monitor.socket.id).emit('PLAYER_DISCONNECTED', client.clientId);
      }
    });

    // loop through and attach event handlers
    Object.keys(handlers).map((key) => socket.on(key, handlers[key]));
    socket.emit('CONNECTED', clientId);
  };

  const init = () => {
    io.on('connection', clientConnected);
  };

  return {
    init,
  };
};

module.exports = Dispatcher();
