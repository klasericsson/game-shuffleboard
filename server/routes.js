const { app } = require('./server');
const path = require('path');

app.get('/', (_, res) => res.sendFile(path.resolve('./public/controller.html')));

app.get('/monitor', (req, res) => res.sendFile(path.resolve('./public/monitor.html')));
