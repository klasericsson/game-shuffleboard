/* global window */
import config from '../../shared/utils/config';

const io = require('socket.io-client');

export const socket = io(window.location.origin, { autoConnect: true });
let clientId = config.clientId;

// export const connect = () => {
//   socket.open();
// };

export const sendMessage = (eventName, payload = {}) =>
  socket.emit('BROADCAST', Object.assign({}, payload, { eventName, clientId }));

// sending to all clients in 'game' room(channel) except sender
export const sendMessageToAll = (eventName, payload) => {
  socket.emit('ROOM_BROADCAST', Object.assign({}, payload, { eventName, clientId, roomId: config.monitorClientId }));
};

export const sendMessageTo = (id, eventName, payload = {}) =>
  sendMessage(eventName, Object.assign({}, payload, { recipient: id }));

export const sendRequest = (eventName, payload = {}, emitEventName = false) => new Promise((resolve, reject) => {
  socket.emit(emitEventName ? eventName : 'CLIENT_REQUEST', Object.assign({}, payload,
  { eventName, clientId }), responseString => {
    const response = JSON.parse(responseString);
    if (response.error) {
      console.log('socket error!!');
      reject(response.error);
    } else {
      resolve(response.content);
    }
  });
});

export const sendRequestTo = (id, eventName, payload = {}, emitEventName = false) =>
    sendRequest(eventName, Object.assign({}, payload, { recipient: id }), emitEventName);

export const renameClient = ({ oldId = clientId, newId }) =>
  sendRequest('RENAME_CLIENT', { oldId, newId, socketId: socket.id, monitorClientId: config.monitorClientId }, true)
  .then(returnedId => {
    clientId = returnedId;
    config.set('clientId', returnedId);
    window.document.title += ` - ${returnedId}`;
    return returnedId;
  });


export const makeSureConnected = new Promise((connectedResolve, connectedReject) => {
  socket.on('CONNECTED', (clientIdFromServer) => {
    console.log(`got clientid ${clientIdFromServer}`);
    // if we already have a client id (e.g on reconnect), we need to update the dispatcher
    if (clientId) {
      renameClient({ oldId: clientIdFromServer, newId: clientId }).then(() => connectedResolve())
      .catch(error => {
        console.log(error);
        connectedReject(`Error connecting. Have you got multiple windows open? <br><br>Details: ${error}`);
      });
    } else {
      clientId = clientIdFromServer;
      config.set('clientId', clientId);
      window.document.title += ` - ${clientId}`;
      connectedResolve();
    }
  });
});
