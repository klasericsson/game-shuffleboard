class MidiKeySprite extends Phaser.Sprite {
	constructor(game, { hue = '255', scale = 1, textureKey = 'textureKey' } = {}) {
		super(game, game.width/2, game.height/2);
		this.textureSize = 64;
		this.initialLifespan = 5000;
		this.lifespan = this.initialLifespan;
		this.blendMode = PIXI.blendModes.ADD;
		this.anchor.setTo(0.3, 0.5);
		this.game.physics.arcade.enable(this);
		this.reset(textureKey, hue, scale);
	}

	update() {
		//super.update();
	}

	reset(textureKey, hue, scale) {
		super.reset(this.game.width/2, this.game.height/2);
		this.lifespan = this.initialLifespan;
		if(!this.game.cache.checkBitmapDataKey(textureKey)) {
			let texture = new MidiKeyBitmap(this.game, textureKey, this.textureSize, this.textureSize, hue);
			this.game.cache.addBitmapData(textureKey, texture);
			console.log('saving texture TO cache');
		} else {
			console.log('loading texture FROM cache');
		}
		this.loadTexture(this.game.cache.getBitmapData(textureKey));
		this.body.velocity.x = this.game.rnd.between(-50, 50);
		this.body.velocity.y = this.game.rnd.between(-500, 50);
		//this.body.angularVelocity = this.game.rnd.between(-50, 50);
		this.body.gravity.y = -this.body.velocity.y;
		this.body.gravity.x = -4*this.body.velocity.x;
		this.scale.setTo(1);
		this.alpha = 1;
		var alphaTween = this.game.add.tween(this).to( { alpha: 0 }, this.initialLifespan, Phaser.Easing.Ease, true);
		var scaleTween = this.game.add.tween(this.scale).to( { x: 10, y: 10 }, this.initialLifespan, Phaser.Easing.Ease, true);
	}

	fade() {

	}

	kill() {
		super.kill();
	}
}

// export default MidiKeySprite;
