/* global Phaser, PIXI */

class PlayerSprite extends Phaser.Sprite {
  constructor(game, x, y, color = '#f44336') {
    super(game, x, y);
    const textureSize = 128;
    const circleSize = 84;
    const lineWidth = 6;
    // this.blendMode = PIXI.blendModes.ADD;
    this.anchor.setTo(0.5, 0.5);
    
    const bmd = game.make.bitmapData(textureSize, textureSize);
    const { context } = bmd;
    context.beginPath();
    context.arc(textureSize / 2, textureSize / 2, (circleSize / 2), 0, 2 * Math.PI, false);
    context.fillStyle = color;
    context.shadowBlur = 12;
    context.shadowColor = color;
    context.fill();
    this.loadTexture(bmd);
    // context.lineWidth = lineWidth;
    // context.strokeStyle = 'rgba(255, 0, 0, 0.8)';
    // context.stroke();
    // const test2 = game.make.sprite(0, 0, bmd);
    // this.addChild(test2);

    // const alphaTween = game.add.tween(test2).to({ alpha: 0.3 }, 500, Phaser.Easing.EaseOut, true).loop(true);
    // alphaTween.yoyo(true, 0); // wait 1000ms

    this.game.physics.p2.enable(this, true);
    this.body.clearShapes();
    this.body.data.physicsShape = this.body.setCircle(circleSize / 2);
    this.body.damping = 0.8;
    this.body.collideWorldBounds = false;
    this.body.velocity.x = 0;
    this.body.velocity.y = 0;
    this.body.mass = 5;
    this.checkWorldBounds = true;
    
    // inner circle
    // const bmd2 = game.make.bitmapData(512, 512);
    // bmd2.context.beginPath();
    // bmd2.context.arc(512/2, 512/2, 50, 0, 2 * Math.PI, false);
    // bmd2.context.fillStyle = 'rgba(0, 255, 0, 0.3)';
    // bmd2.context.shadowBlur = 16;
    // bmd2.context.shadowColor = '#00ff00';
    // bmd2.context.lineWidth = 45;
    // bmd2.context.fill();
    // // bmd2.context.strokeStyle = 'rgba(0, 255, 0, 0.5)';
    // // bmd2.context.stroke();
    // const test3 = game.make.sprite(-this.textureSize/4, -this.textureSize/4, bmd2);
    // this.addChild(test3);
  }

}

export default PlayerSprite;
