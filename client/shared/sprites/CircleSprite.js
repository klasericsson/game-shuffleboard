import CircleBitmap from '../bitmaps/CircleBitmap';

const defaults =  {

};

class CircleSprite extends Phaser.Sprite {
  constructor(game, {
    hue = '255',
    scale = 0.5,
    textureKey = `circleHue${hue}`,
    lifespan = 0,
    enablePhysics = true,
    scaleFactor = 20,
  } = {}) {
    super(game, game.width/2, game.height/2);
    this.textureSize = 128;
    this.hue = hue;
    this.initialScale = scale;
    this.scaleFactor = scaleFactor;
    if (lifespan > 0) {
      this.initialLifespan = lifespan;
      this.lifespan = this.initialLifespan;
    }
    this.blendMode = PIXI.blendModes.ADD;
    this.anchor.setTo(0.5, 0.5);
    if (enablePhysics) {
      this.game.physics.p2.enable(this);
    }
    this.reset(textureKey, hue, scale);
  }

  update() {
  //super.update();
  }

  reset(textureKey, hue, scale) {
    super.reset(this.game.width/2, this.game.height/2);
    // this.lifespan = this.initialLifespan;
    if (!this.game.cache.checkBitmapDataKey(textureKey)) {
      let texture = new CircleBitmap(this.game, textureKey, this.textureSize, this.textureSize, hue);
      this.game.cache.addBitmapData(textureKey, texture);
      console.log('saving texture TO cache');
    } else {
      // console.log('loading texture FROM cache');
    }
    this.loadTexture(this.game.cache.getBitmapData(textureKey));
    // this.body.velocity.x = this.game.rnd.between(-50, 50);
    // this.body.velocity.y = this.game.rnd.between(-500, 50);
    // this.body.angularVelocity = this.game.rnd.between(-50, 50);
    // this.body.gravity.y = -this.body.velocity.y;
    // this.body.gravity.x = -4*this.body.velocity.x;
    this.scale.setTo(this.initialScale);
    this.alpha = 1;
    if (this.initialLifespan) {
      const alphaTween = this.game.add.tween(this).to( { alpha: 0 }, this.initialLifespan, Phaser.Easing.Ease, true);
      const scaleTween = this.game.add.tween(this.scale).to(
        {
          x: this.initialScale * this.scaleFactor,
          y: this.initialScale * this.scaleFactor
        },
        this.initialLifespan, Phaser.Easing.Ease, true);
    }
  }

  fade() {

  }

  kill() {
    super.kill();
  }
}

export default CircleSprite;
