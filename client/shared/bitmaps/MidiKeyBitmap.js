class MidiKeyBitmap extends Phaser.BitmapData {
	constructor(game, key, width, height, hue) {
	super(game, key, width, height);
	let radius = width/2;
	let colour = hue + ', 100%, 50%';

	// fill gradient
	let grd = this.context.createRadialGradient(radius, radius, 1, radius, radius, radius);
	let red = this.game.rnd.pick([255,255]);
	let green = this.game.rnd.pick([0,255]);
	let blue = this.game.rnd.pick([0,255]);

	let hslaColour = 'hsla(' + colour;
	grd.addColorStop(0.2, hslaColour + ',0.8)');
	grd.addColorStop(0.5, hslaColour + ',0.5)');
	grd.addColorStop(1, 'hsla(0,0%,0%,0)');

	this.context.fillStyle = grd;
	this.context.arc(radius, radius, radius, 0, Math.PI * 2);
	this.context.fill();
	}
}

export default MidiKeyBitmap;