/* global window */

export const getResponsiveSize = (object, percent) => {
  const ratio = object.width / object.height;
  const width = Math.round((percent / 100) * window.innerWidth);
  const height = Math.round(width / ratio);
  return { width, height };
};

export const createResponsiveBitmapFromCache = (key, percent, game) => {
  const size = getResponsiveSize(game.cache.getImage(key), percent);
  const bitmap = game.make.bitmapData(size.width, size.height);
  // copy the image from cache and scale it to fit the canvas
  bitmap.copy(key, 0, 0, null, null, null, null, size.width, size.height);
  return bitmap;
};

export const createBitmapDataWithImage = (game, key, width, height) => {
  const bitmap = game.make.bitmapData(width, height);
  // copy the image from cache and scale it to fit the canvas
  bitmap.copy(key, 0, 0, null, null, null, null, width, height);
  return bitmap;
};

export const center = (object, world) => {
  const obj = object;
  obj.x = world.centerX - (obj.width / 2);
  obj.y = world.centerY - (obj.height / 2);
};
