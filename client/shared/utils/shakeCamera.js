const shakeCamera = (game, options = {}) => {
  const { camera } = game;
  const settings = Object.assign({
    shakeRange: 5,
    shakeCount: 1,
    shakeInterval: 15,
    randomShake: false,
    randomizeInterval: true,
    shakeAxis: 'xy',
  }, options);
  camera.bounds = null;

  const tempCount = settings.shakeCount;
  const shakeTimer = game.time.create(false);
  const cameraCurrentPosition = game.camera.position;

  shakeTimer.loop(settings.randomizeInterval ? ((Math.random() * settings.shakeInterval) +
  settings.shakeInterval) : settings.shakeInterval, () => {
    if (settings.shakeCount === 0) {
      // if shake end set camera to default position
      camera.position = cameraCurrentPosition;
      // stop shake timer
      shakeTimer.stop();
      // restore value of shakeCount
      settings.shakeCount = tempCount;
      return;
    }

    let shift1;
    let shift2;

    if (settings.randomShake) {
      // if uneven shifts in the coordinates
      shift1 = this.game.rnd.integerInRange(-settings.shakeRange / 2, settings.shakeRange / 2);
      shift2 = this.game.rnd.integerInRange(-settings.shakeRange / 2, settings.shakeRange / 2);
    } else {
      // If regular shifts
      if (settings.shakeCount % 2) {
        shift1 = shift2 = -settings.shakeRange / 2;
      } else {
        shift1 = shift2 = settings.shakeRange / 2;
      }
    }

    if (settings.shakeAxis !== 'y') camera.x += shift1;
    if (settings.shakeAxis !== 'x') camera.y += shift2;

    settings.shakeCount--;
  }, this);

  shakeTimer.start();
};

export default shakeCamera;
