/* global Phaser, document, window, localStorage */

export const isOutsideWorld = (game, sprite) => {
  
};

export const isMoving = (sprite) => {
  if (Phaser.Point.equals(sprite.body.velocity, new Phaser.Point(0,0))) {
    return false;
  }
  return true;
};

export const isLocalStorageNameSupported = () => {
  try {
    const testKey = 'test';
    localStorage.setItem(testKey, '1');
    localStorage.removeItem(testKey);
    return true;
  } catch (e) {
    return false;
  }
};

export const msToTimeString = (ms) => {
  let milliseconds = ms;
  const hours = Math.floor(milliseconds / 3600000);
  milliseconds -= hours * 3600000;

  let minutes = Math.floor(milliseconds / 60000);
  milliseconds -= minutes * 60000;
  if (minutes < 10) minutes = `0${minutes}`;

  let seconds = Math.floor(milliseconds / 1000);
  milliseconds -= seconds * 1000;
  if (seconds < 10) seconds = `0${seconds}`;

  let hundredths = Math.floor(milliseconds / 10);
  if (hundredths < 10) hundredths = `0${hundredths}`;

  return `${minutes}:${seconds}:${hundredths}`;
};

export const msToSecondString = (ms) => {
  const milliseconds = ms;
  let seconds = Math.floor(milliseconds / 1000);
  if (seconds < 10) seconds = `0${seconds}`;
  return seconds;
};

export const ui = {
  hide: (el) => {
    el.classList.add('ui__window--hidden');
  },
  show: (el) => {
    document.querySelector('.ui').style.visibility = 'visible';
    document.querySelector('.ui').style.opacity = 1;
    el.classList.remove('ui__window--hidden');
  },
  hideAll: () => {
    document.querySelectorAll('.ui__window').forEach(el => el.classList.add('ui__window--hidden'));
    document.querySelector('.ui').style.visibility = 'hidden';
    document.querySelector('.ui').style.opacity = 0;
  },
  windows: {
    join: document.querySelector('.ui__window.join'),
    waiting: document.querySelector('.ui__window.waiting'),
    help: document.querySelector('.ui__window.help'),
    team: document.querySelector('.ui__window.team'),
  },
};

document.querySelectorAll('button.close').forEach(btn => {
  btn.addEventListener('click', () => {
    ui.hide(ui.windows[btn.getAttribute('data-window')]);
  });
});

export const getUrlParam = (name, url) => {
  const theUrl = url || window.location.href;
  let theName = name;
  theName = theName.replace(/[\[]/, "\\\[").replace(/[\]]/,"\\\]");
  const regexS = '[\\?&]"+theName+"=([^&#]*)';
  const regex = new RegExp(regexS);
  const results = regex.exec(theUrl);
  return results == null ? null : results[1];
};
