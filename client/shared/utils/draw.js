export const line = (startPoint, endPoint, context) => {
  const ctx = context;
  ctx.lineWidth = 10;
  ctx.strokeStyle = 'rgba(0, 255, 0, 0.5)';
  ctx.clearRect(0, 0, this.game.width, this.game.height);
  ctx.beginPath();
  ctx.setLineDash([15, 5]);
  ctx.moveTo(startPoint.x, startPoint.y);
  ctx.lineTo(endPoint.x, endPoint.y);
  ctx.stroke();
};
