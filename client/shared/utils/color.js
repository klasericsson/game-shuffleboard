export const colors = {
  red: { hex: '#f44336', rgba: '' },
  blue: { hex: '#2196f3', rgba: '' },
  yellow: { hex: '#ffeb3b', rgba: '' },
  green: { hex: '#4caf50', rgba: '' },
};
