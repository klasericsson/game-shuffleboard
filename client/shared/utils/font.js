export const defaultStyle = {
	font: '32px Segoe',
	fill: '#fff',
	wordWrap: false,
	align: 'center',
};

export const mobileStyle = {
	font: '32px Noto Sans',
	fill: '#fff',
	wordWrap: false,
	align: 'center',
};
