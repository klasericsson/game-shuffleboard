/* global localStorage */
// TODO: Check if localstorage is enabled (i.e not in private mode) and alert
let config = JSON.parse(localStorage.getItem('config')) || {};

const set = (key, object) => {
  if (typeof key === 'object') {
    config = Object.assign(config, key);
  } else {
    config[key] = object;
  }
  localStorage.setItem('config', JSON.stringify(config));
};
config.set = set;

export default config;
