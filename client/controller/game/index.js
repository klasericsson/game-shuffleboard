/* global document */
import Phaser from 'phaser';
import BootState from '../states/Boot';
import JoinState from '../states/Join';
import ShuffleboardLevel from '../levels/shuffleboard';

class Game extends Phaser.Game {

  constructor() {
    super(document.documentElement.clientWidth,
        document.documentElement.clientHeight, Phaser.AUTO, 'content', null);

    this.data = {
      currentPlayer: {
        name: '',
        team: 'red',
        color: '#0ff00',
      },
      myTurn: false,
    };
    this.state.add('Boot', BootState);
    this.state.add('Join', JoinState);
    this.state.add('ShuffleboardLevel', ShuffleboardLevel);
    this.state.start('Boot');
  }
}

export default Game;
