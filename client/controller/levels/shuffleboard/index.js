import Phaser from 'phaser';

// import { center } from '../../../shared/utils/responsive';
import throttle from 'lodash.throttle';
import { socket, sendRequestTo, sendMessageToAll } from '../../../shared/socket';
import config from '../../../shared/utils/config';
import { ui } from '../../../shared/utils/helpers';
import { mobileStyle } from '../../../shared/utils/font';
import PlayerSprite from '../../../shared/sprites/PlayerSprite';

const isPrimaryPointer = pointer => pointer.id === 1 || pointer.id === 0;
const sendMessageToAllThrottled = throttle(sendMessageToAll, 25, { leading: true, trailing: false });

let canSwipe = false;

export default class Shuffleboard extends Phaser.State {

  init() {
    ui.hideAll();
    this.input.maxPointers = 2;
    this.input.recordPointerHistory = true;
    this.input.recordRate = 20;
    this.input.recordLimit = 10;
    this.swipe = {
      start: null,
      end: null,
      history: [],
      minMove: 10,
      direction: { x: 'none', y: 'none' },
      lastDirection: { x: 'none', y: 'none' },
    };
  }

  preload() {

  }

  create() {
    const { game } = this;

    this.world.setBounds(0, 0, game.width, game.height);

    this.bindEvents();
    this.bindSocketEvents();
    this.physics.startSystem(Phaser.Physics.P2JS);
    // this.puck = this.add.sprite(this.world.centerX, this.world.centerY, 'puck');
    // this.physics.p2.enable([this.puck], true);
    // this.puck.body.clearShapes();
    // this.puck.body.setCircle(45);
    // this.puck.body.collideWorldBounds = false;
    // window.puck = this.puck;
    const statusTextStyle = Object.assign({}, mobileStyle, { font: '32px "Roboto Condensed"' });
    this.statusText = this.add.text(game.world.centerX, game.world.centerY, '', statusTextStyle);
    this.statusText.anchor.set(0.5, 0.5);

    setTimeout(() => {
      this.game.data.myTurn = true;
      this.spawnPuck();
    }, 500);
  }


  shutdown() {

  }

  checkPuckBounds() {

  }

  update() {
    if (!this.game.data.myTurn) {
      this.statusText.alpha = 1;
      this.statusText.setText(`Current player:\n${this.game.data.currentPlayer.name}`);
    } else {
      this.statusText.alpha = 0;
      if (this.puck) {
        this.checkPuckBounds();
      }
    }


    if (canSwipe && this.swiping) {
      // console.log(this.input.x, this.input.y);
      this.puck.body.x = this.input.x;
      this.puck.body.y = this.input.y;
      sendMessageToAllThrottled('PUCK_MOVE', { x: this.puck.body.x, y: this.puck.body.y });
      if (this.input.activePointer._history.length > 4) {
        this.updateSwipe();
      }
    }
  }

  bindSocketEvents() {
    const { game, statusText, spawnPuck } = this;
    socket.on('CURRENT_PLAYER', (response) => {
      if (response.currentPlayer.clientId === config.clientId) {
        game.data.myTurn = true;
        // spawnPuck();
      } else {
        game.data.myTurn = false;
        game.data.currentPlayer = response.player;
        statusText.addColor(response.player.team, 15);
      }
    });
  }

  spawnPuck() {
    canSwipe = true;
    // this.puck = new PlayerSprite(this.game, this.world.centerX, this.world.centerY);
    // window.puck = this.puck;
    const { game } = this;
    this.puck = new PlayerSprite(game, game.world.centerX, game.world.centerY, game.data.color);
    const { puck } = this;
    this.game.add.existing(puck);
    puck.events.onOutOfBounds.addOnce(() => {
      // console.log('VELOCITY:');
      // console.log(puck.body.velocity);
      // console.log('POSITION:');
      // console.log(puck.position);
      // console.log('DELTAX:');
      // console.log(puck.position.x - game.world.centerX);
      sendRequestTo(config.monitorClientId, 'SPAWN_PUCK', {
        position: puck.position,
        velocity: puck.velocity,
        deltaX: puck.position.x - game.world.centerX,
      })
      .then(response => {
        // console.info(response);
      })
      .catch(console.error);
      canSwipe = false;
      puck.destroy();
    });
    
    // this.physics.p2.enable([this.puck], true);
    // this.puck.body.clearShapes();
    // this.puck.body.setCircle(45);
    // this.puck.body.collideWorldBounds = false;
    // window.puck = this.puck;
  }

  bindEvents() {
    this.input.onDown.add(pointer => {
      if (canSwipe && isPrimaryPointer(pointer)) {
        this.swiping = true;
        this.swipe.start = {
          x: {
            time: Date.now(),
            pos: pointer.x,
          },
          y: {
            time: Date.now(),
            pos: pointer.y,
          },
        };
      }
    });
    this.input.onUp.add(pointer => {
      if (canSwipe && isPrimaryPointer(pointer)) {
        this.swiping = false;
        const { swipe, puck } = this;
        swipe.end = {
          x: {
            time: Date.now(),
            pos: pointer.x,
          },
          y: {
            time: Date.now(),
            pos: pointer.y,
          },
        };
        swipe.velocity = {
          x: (swipe.end.x.pos - swipe.start.x.pos) / ((swipe.end.x.time - swipe.start.x.time) / 1000),
          y: (swipe.end.y.pos - swipe.start.y.pos) / ((swipe.end.y.time - swipe.start.y.time) / 1000),
        };

        if (puck) {
          puck.body.velocity.x = swipe.velocity.x;
          puck.body.velocity.y = swipe.velocity.y;
        }
      }
    });
  }

  updateSwipe() {
    const { activePointer } = this.input;
    const { _history } = activePointer;
    const { swipe } = this;
    const l = _history.length;
    swipe.direction.x = 'none';
    if (activePointer.x - _history[l - 1].x < 0) {
      swipe.direction.x = 'left';
    } else if (activePointer.x - _history[l - 1].x > 0) {
      swipe.direction.x = 'right';
    }
    swipe.direction.y = 'none';
    if (activePointer.y - _history[l - 1].y < 0) {
      swipe.direction.y = 'up';
    } else if (activePointer.y - _history[l - 1].y > 0) {
      swipe.direction.y = 'down';
    }

    if (swipe.direction.x !== swipe.lastDirection.x) {
      console.log('resetX!');
      swipe.start.x = {
        time: Date.now(),
        pos: activePointer.x,
      };
    }
    if (swipe.direction.y !== swipe.lastDirection.y) {
      console.log('resetY!');
      swipe.start.y = {
        time: Date.now(),
        pos: activePointer.y,
      };
    }
    swipe.lastDirection.x = swipe.direction.x;
    swipe.lastDirection.y = swipe.direction.y;
  }

  render() {
    // this.game.debug.text(`Pointer1: id: ${this.input.pointer1.id} ${this.input.pointer1.isDown}`, 40, 40);
    // this.game.debug.text(`Pointer2: id: ${this.input.pointer2.id} ${this.input.pointer2.isDown}`, 40, 80);
    // this.game.debug.text(`xDirection: ${this.swipe.direction.x}`, 40, 120);
    // this.game.debug.text(`yDirection: ${this.swipe.direction.y}`, 40, 160);
  }

}
