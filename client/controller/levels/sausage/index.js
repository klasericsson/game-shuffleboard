import Phaser from 'phaser';
import sausage from '../../../shared/levels/sausage/objects/Sausage';
import { center } from '../../../shared/utils/responsive';
import { socket, sendRequestTo } from '../../../shared/socket';
import config from '../../../shared/utils/config';

const assetsPath = './assets/levels/sausage/';

export default class SausageLevel extends Phaser.State {

  init() {
    this.input.maxPointers = 1;
    this.sliceDrag = {
      start: null,
      end: null,
    };
    this.canCut = true;
    this.sliceLineColour = 'rgba(255, 0, 0, 0.5)';
    this.sliceLineValidColour = 'rgba(0, 255, 0, 0.5)';
    this.tweens = {
      knife: {},
    };
    this.bindEvents();
  }

  bindEvents() {
    this.input.onDown.add((pointer) => {
      if (this.canCut) {
        this.startCut(pointer.worldX, pointer.worldY);
      }
    });
    this.input.onUp.add((pointer) => {
      if (this.sliceDrag.start) {
        this.handlePointerUp(pointer);
      }
    });

    socket.on('OK_TO_SLICE', (payload) => {
      this.canCut = true;
      console.log(payload);
    });
  }

  preload() {
    this.load.image('knife', `${assetsPath}images/meat-knife.png`);
    this.load.image('starburst', `${assetsPath}images/starburst.png`);
    this.load.audio('knife-slash', [`${assetsPath}audio/knife-slash.mp3`]);
    for (const asset of sausage.assets) {
      this.load.image(asset.key, asset.path);
    }
  }

  create() {
    this.starburst = this.add.image(this.world.centerX, this.world.centerY, 'starburst');
    // this.starburst.pivot.x = this.world.centerX;
    // this.starburst.pivot.y = this.world.centerY;
    this.starburst.anchor.set(0.5, 0.5);

    this.sausage = sausage.create(this.game, 0, 0, this.world.width * 0.9);
    center(this.sausage.sprite, this.world);
    // this.sausage.sprite.alpha = 0;
    this.dragLine = this.make.bitmapData(this.world.width, this.world.height);
    this.add.image(0, 0, this.dragLine);

    this.sounds = {
      slice: this.game.add.audio('knife-slash'),
    };

    this.knife = this.game.add.sprite(0, 100, 'knife');
    // this.knife.alpha = 0;
    this.knife.anchor.setTo(0.2, 0.5);
  }

  handlePointerUp(pointer) {
    this.sliceDrag.end = new Phaser.Point(pointer.worldX, pointer.worldY);
    const xPos = this.sliceDrag.start.x - this.sausage.sprite.x;
    if (this.canCut) {
      if (this.sliceValid(xPos)) {
        this.makeCut(xPos);
      } else {
        this.hideKnife();
      }
      this.dragLine.clear();
    }
  }

  clearTweens(key) {
    Object.keys(this.tweens[key]).forEach(k => {
      this.tweens[key][k].stop();
    });
  }

  showKnife() {
    this.clearTweens('knife');
    this.tweens.knife.fadeIn = this.game.add.tween(this.knife).to({ alpha: 1 }, 100, Phaser.Easing.Sinusoidal.In, true);
  }

  hideKnife() {
    this.tweens.knife.fadeOut = this.game.add.tween(this.knife).to({ alpha: 0 },
      200, Phaser.Easing.Sinusoidal.In, true);
  }

  startCut(x, y) {
    this.knife.x = x;
    this.knife.y = y;
    this.showKnife();
    this.sliceDrag = {
      start: new Phaser.Point(x, y),
    };
  }

  makeCut(xPos) {
    this.sausage.slice(xPos);
    this.sounds.slice.play();
    this.tweens.knife.cut = this.game.add.tween(this.knife).to(
      { y: this.sliceDrag.end.y, angle: -25 },
      100, Phaser.Easing.Quadratic.In, true
    );
    this.tweens.knife.cut.onComplete.add(() => this.game.camera.shake(0.01, 50));
    this.tweens.knife.fadeOut = this.game.add.tween(this.knife).to(
      { alpha: 0, x: this.game.width + this.knife.width },
      300, Phaser.Easing.Sinusoidal.Out, true, 400);
    sendRequestTo(config.monitorClientId, 'SLICE_SAUSAGE', { xPercent: xPos / this.sausage.sprite.width })
    .then(response => {
      console.info(response);
    })
    .catch(console.error);
  }

  drawLine(startPoint, endPoint, color = 'rgba(255, 0, 0, 0.5)') {
    const { context } = this.dragLine;
    context.lineWidth = 4;
    context.strokeStyle = color;
    context.clearRect(0, 0, this.game.width, this.game.height);
    context.beginPath();
    context.setLineDash([15, 5]);
    context.moveTo(startPoint.x, startPoint.y);
    context.lineTo(startPoint.x, endPoint.y);
    context.stroke();
    this.dragLine.dirty = true;
  }

  sliceValid(width) {
    const sausageBounds = this.sausage.sprite.getBounds();
    if (width && this.sliceDrag.start.y < sausageBounds.y
      && this.sliceDrag.end.y > sausageBounds.y + sausageBounds.height
      && this.sausage.canSlice(width)) {
      return true;
    }
    return false;
  }

  shutdown() {
    // bitmapdata.destroy();
  }

  update() {
    this.starburst.rotation += 0.005;
  }

  render() {
    if (this.canCut && this.input.activePointer.isDown) {
      this.sliceDrag.end = new Phaser.Point(
      this.sliceDrag.start.x, this.input.activePointer.worldY);
      this.drawLine(this.sliceDrag.start, this.sliceDrag.end,
        this.sliceValid(this.sliceDrag.start.x - this.sausage.sprite.x) ?
        this.sliceLineValidColour : this.sliceLineColour);
      this.knife.visible = true;
      this.knife.x = this.sliceDrag.start.x;
      this.knife.angle = (this.sliceDrag.end.y - this.sliceDrag.start.y) * 0.01;
      this.knife.y = this.sliceDrag.start.y - ((this.sliceDrag.end.y - this.sliceDrag.start.y) * 0.3);
    }
  }

}
