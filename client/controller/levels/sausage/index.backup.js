import Phaser from 'phaser';

const assetsPath = './assets/levels/sausage/';

export default class Sausage extends Phaser.State {

  init() {}

  preload() {
    this.load.image('knife', `${assetsPath}images/meat-knife.png`);
    this.load.image('sausage', `${assetsPath}images/sausage-horisontal.png`);
    this.load.image('sausage-lid', `${assetsPath}images/sausage-horisontal-lid.png`);
    this.load.audio('knife-slash', [`${assetsPath}audio/knife-slash.mp3`]);
  }

  create() {
    console.log('Sausage create');
    console.log(`Game name: ${this.game.x.name}`);
    const { game } = this;
    this.game.input.maxPointers = 1;

    this.sounds = {
      slice: game.add.audio('knife-slash'),
    };

    this.sliceLineColour = 'rgba(255, 0, 0, 0.5)';
    this.sliceLineValidColour = 'rgba(0, 255, 0, 0.5)';

    this.knife = game.add.sprite(200, 200, 'knife');
    this.knife.anchor.setTo(0.2, 0.5);
    this.knife.visible = false;

    //this.sausageLid = game.add.sprite(game.width / 2, game.height / 2, 'sausage-lid');

    this.sausage = game.add.sprite(0, game.height / 2, 'sausage');
    const ratio = this.sausage.width / this.sausage.height;
    this.sausage.anchor.setTo(0, 0.5);
    this.sausage.width = game.width;
    this.sausage.height = this.sausage.width / ratio;

    this.sausageMaskBitmap = game.make.bitmapData(this.sausage.width, this.sausage.height);
    //this.game.add.sprite(0, 0, this.sausageMaskBitmap).anchor.setTo(0, 0);

    this.line = game.make.bitmapData(game.width, game.height);
    this.game.add.sprite(0, 0, this.line);

    game.input.onDown.add((pointer) => {
      console.log('DOWN!');
      this.knife.angle = 0;
      this.knife.alpha = 1;
      this.slice = {
        start: new Phaser.Point(pointer.worldX, pointer.worldY),
      };
    });
    game.input.onUp.add(() => {
      if (this.sliceValid()) {
        //this.doSlice();
        this.drawMask();
      }
    });
  }

  doSlice() {
    //this.sounds.slice.play();
    this.game.add.tween(this.knife).to(
      { y: this.slice.end.y, angle: -25 },
      100, Phaser.Easing.Quadratic.In, true);
    this.game.add.tween(this.knife).to(
      { alpha: 0, x: this.game.width + this.knife.width },
      300, Phaser.Easing.Sinusoidal.Out, true, 400);
  }

  drawLine(startPoint, endPoint, color = '#ff0000') {
    const { context } = this.line;
    context.lineWidth = 5;
    context.strokeStyle = color;
    context.clearRect(0, 0, this.game.width, this.game.height);
    context.beginPath();
    context.setLineDash([15, 5]);
    context.moveTo(startPoint.x, startPoint.y);
    context.lineTo(startPoint.x, endPoint.y);
    context.stroke();
    this.line.dirty = true;
  }

  drawMask() {
    const { context } = this.sausageMaskBitmap;
    context.clearRect(0, 0, this.sausage.width, this.sausage.height);
    context.fillStyle = 'rgba(0, 0, 0, 1)';
    context.beginPath();
    context.moveTo(this.slice.start.x, 0);
    context.lineTo(this.slice.start.x, this.sausage.height);
    context.lineTo(this.sausage.width, this.sausage.height);
    context.lineTo(this.sausage.width, 0);
    context.closePath();
    context.fill();

    const bmd = this.game.make.bitmapData(900, this.sausage.height);
    bmd.alphaMask(this.sausage, this.sausageMaskBitmap);
    //bmd.replaceRGB(0, 255, 0, 255, 0, 255, 0, 255);
    //bmd.update();
    //bmd.addToWorld(0, 0, 100, 0.5);
    this.game.add.image(0, 300, bmd).anchor.set(0, 0.5);

    //const imageData = bmd.context.getImageData(0, 0, this.game.width, this.game.height);
    const imageData = { data: [] };
    // examine every pixel,
    // change any old rgb to the new-rgb
    for (let i = 0; i < imageData.data.length; i += 4) {
      // is this pixel the old rgb?
      if ((imageData.data[i] === 0 &&
         imageData.data[i + 1] === 0 &&
         imageData.data[i + 2] === 0) || imageData[i + 3] < 255
      ) {
        // change to your new rgb
        imageData.data[i] = 255;
        imageData.data[i + 1] = 255;
        imageData.data[i + 2] = 0;
        imageData.data[i + 3] = 255;
      }
    }
    // put the altered data back on the canvas
    //context.putImageData(imageData, 0, 0);

    //bmd.replaceRGB(0, 255, 0, 255, 0, 255, 0, 255);
    setTimeout(() => {
      console.log('replacing');
      //bmd.replaceRGB(255, 255, 200, 255, 20, 255, 40, 255);
      //bmd.update();
    }, 2000);
  }

  sliceValid() {
    if (this.slice.start.y < this.sausage.y - (this.sausage.height / 2)
      && this.slice.end.y > this.sausage.y + (this.sausage.height / 2)) {
      return true;
    }
    return false;
  }

  render() {
    const { activePointer } = this.game.input;
    //this.game.debug.pointer(activePointer);
    if (activePointer.isDown) {
      const { worldX, worldY } = activePointer;
      this.knife.visible = true;
      this.knife.x = this.slice.start.x;
      this.slice.end = new Phaser.Point(this.slice.start.x, worldY);
      this.knife.angle = (this.slice.end.y - this.slice.start.y) * 0.01;
      this.knife.y = this.slice.start.y - ((this.slice.end.y - this.slice.start.y) * 0.3);

      this.drawLine(this.slice.start, this.slice.end, this.sliceValid() ?
        this.sliceLineValidColour : this.sliceLineColour);

      //const line = new Phaser.Line(this.slice.start.x, this.slice.start.y, worldX, worldY);

    } else {
      //this.knife.visible = false;
      //this.drawLine(new Phaser.Point(10, 10), new Phaser.Point(100, 100));
    }
  }

}
