/* global window, document */
import 'pixi';
import 'p2';
import Game from './game';
import { isLocalStorageNameSupported } from '../shared/utils/helpers';

let game;
if (isLocalStorageNameSupported()) {
  game = new Game();
} else {
  window.location.href = `/error.html?msg=${encodeURIComponent(
    `
      It looks like you're in private browsing mode.
      <br></br>Please turn off private browsing to play.
    `
  )}`;
}

window.game = game;

/*
import { sendMessage, onReceiveMessage } from '../shared/socket';
import { messages } from '../shared/levels/sausage/messages';

console.log('controller');

// const monitorClientId = window.location.hash.substr(1);

const explainingText = document.createElement('p');
explainingText.appendChild(
  document.createTextNode(`
    Tell the monitor how much of your dear sausage you want to slice!
    Give the server a number between 0-100. When you're out of sausage,
    the monitor will tell you that you lost!
  `)
);

const input = document.createElement('input');
input.setAttribute('type', 'text');

const button = document.createElement('button');
button.appendChild(document.createTextNode('send to monitor!'));
button.addEventListener('click', () => sendMessage('SEND_TO_MONITOR', {
  eventName: messages.SLICE,
  payload: { percentageOfSausage: input.value },
}));

document.body.appendChild(explainingText);
document.body.appendChild(input);
document.body.appendChild(button);

setTimeout(() => sendMessage('CONTROLLER_READY'), 1000);
setTimeout(() => sendMessage('JOIN_MONITOR', { monitorName: prompt('password please') }), 2000);

onReceiveMessage('SAUSAGE_LEFT', ({ sausageLeft }) => alert(`${sausageLeft}% left!`));
onReceiveMessage('GAME_OVER', () => alert('you lost loser!'));
onReceiveMessage('ERROR', console.log);
*/
