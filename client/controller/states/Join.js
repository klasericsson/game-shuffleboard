/* global window, document */
import Phaser from 'phaser';
import { makeSureConnected, socket, sendMessageTo, sendRequestTo } from '../../shared/socket';
import config from '../../shared/utils/config';
import { ui } from '../../shared/utils/helpers';


const joinButton = document.getElementById('join-game');
const feedbackEl = document.getElementById('join-feedback');
const nameEl = document.getElementById('name');
const teamButtons = document.querySelectorAll('.teams button');
const readyButton = document.getElementById('ready-button');

export default class extends Phaser.State {
  init() {
    console.log('join state init');
    config.set('monitorClientId', 'monitor');
  }

  create() {
    makeSureConnected.then(() => {
      this.setup();
    }).catch((error) => {
      window.location.href = `/error.html?msg=${encodeURIComponent(error)}`;
    });
  }

  bindEvents() {
    socket.on('GAME_START', (payload) => {
      if (config.playerName && config.team) {
        this.state.start('ShuffleboardLevel');
      }
    });
  }

  setup() {
    this.bindEvents();
    if (!config.playerName) {
      ui.show(ui.windows.help);
      ui.show(ui.windows.join);
    } else {
      nameEl.value = config.playerName;
      ui.hideAll();
      this.joinGame();
    }

    joinButton.addEventListener('click', () => {
      if (nameEl.value.trim()) {
        config.set('playerName', nameEl.value);
        joinButton.disabled = true;
        this.joinGame();
      } else {
        feedbackEl.innerHTML = 'Please enter a name';
      }
    });

    readyButton.addEventListener('click', () => {
      readyButton.disabled = true;
      sendRequestTo(config.monitorClientId, 'PLAYER_READY')
        .then((response) => {
          console.log(response);
          if (response.playerReady) {
            teamButtons.forEach((btn) => {
              btn.disabled = true;
            });
            config.set('ready', true);
            readyButton.style.opacity = 0.25;
            if (response.gameInProgress) {
              this.state.start('ShuffleboardLevel');
            }
          } else {
            config.set('ready', false);
            readyButton.style.opacity = 1;
            teamButtons.forEach((btn) => {
              btn.disabled = false;
            });
          }
          readyButton.disabled = false;
        }).catch((error) => {
          readyButton.disabled = false;
        });
    });

    Array.from(teamButtons).forEach((button) => {
      button.addEventListener('click', () => {
        const team = button.getAttribute('data-team');
        teamButtons.forEach((btn) => {
          btn.classList.add('button--unfocused');
        });
        button.classList.remove('button--unfocused');
        this.chooseTeam(team).then((response) => {
          this.game.data.color = response.color;
          config.set('color', response.color);
          config.set('team', response.team);
          readyButton.style.display = 'block';
        });
      });
    });
  }

  joinGame() {
    console.log('trying to join...');
    sendRequestTo(config.monitorClientId, 'JOIN_MONITOR_REQUEST', {
      name: config.playerName,
    }, true)
    .then(response => {
      // if a player resumes
      if (response.gameInProgress && config.gameId && config.gameId === response.gameId) {
        this.game.data.team = config.team;
        this.game.data.color = config.color;
        this.state.start('ShuffleboardLevel');
      } else {
        ui.show(ui.windows.team);
      }
      config.set('gameId', response.gameId);
    })
    .catch((error) => {
      console.log('in error!');
      console.log(error);
      feedbackEl.innerHTML = error;
      ui.show(ui.windows.join);
    });
  }

  chooseTeam(team) {
    return sendRequestTo(config.monitorClientId, 'CHOOSE_TEAM', { team });
  }

  preload() {

  }

  update() {

  }

  shutdown() {

  }

  render() {

  }

}
