
/* global document */
import Phaser from 'phaser';
import BootState from '../states/Boot';
import SettingsState from '../states/Settings';
import LobbyState from '../states/Lobby';
import ShuffleboardLevel from '../levels/shuffleBoard';
import { socket, sendRequestTo } from '../../shared/socket';

const { response } = require('../../../common/response');

class Game extends Phaser.Game {

  constructor() {
    const width = document.documentElement.clientWidth;
    const height = document.documentElement.clientHeight;
    super(width, height, Phaser.AUTO, 'content', null);
    const game = this;

    this.data = {
      currentPlayer: null,
      gameInProgress: false,
      players: { a: 'b' },
      teams: {
        red: {
          color: '#f44336',
          players: [],
          score: 0,
          turnIndex: 0,
        },
        blue: {
          color: '#2196f3',
          players: [],
          score: 0,
          turnIndex: 0,
        },
        yellow: {
          color: '#ffeb3b',
          players: [],
          score: 0,
          turnIndex: 0,
        },
        green: {
          color: '#4caf50',
          players: [],
          score: 0,
          turnIndex: 0,
        },
      },
      events: {
        playerReady: new Phaser.Signal(),
      },
    };

    this.utils = {
      getTeams: () => Object.keys(game.data.teams).map((key) => game.data.teams[key]),
      getPlayers: () => Object.keys(game.data.players).map((key) => game.data.players[key]),
    };

    this.state.add('Boot', BootState, false);
    this.state.add('Settings', SettingsState, false);
    this.state.add('Lobby', LobbyState, false);
    this.state.add('ShuffleboardLevel', ShuffleboardLevel, false);

    this.bindGlobalSocketEvents();
    this.state.start('Boot');
  }

  bindGlobalSocketEvents() {
    const game = this;
    const { players, teams } = game.data;
    socket.on('JOIN_MONITOR_REQUEST', (data, callback) => {
      console.log(`Join request received from ${data.clientId}`);
      players[data.clientId] = {
        clientId: data.clientId,
        socketId: data.socketId,
        name: data.name,
        active: true,
        ready: false,
        score: 0,
      };
      callback(response.ok({
        gameInProgress: game.data.gameInProgress,
        gameId: game.data.gameId,
        status: 'success',
      }));
    });

    socket.on('PLAYER_DISCONNECTED', (clientId) => {
      const client = players[clientId];
      if (client) {
        client.active = false;
      }
    });

    socket.on('CHOOSE_TEAM', (data, callback) => {
      const team = teams[data.team];
      const player = players[data.clientId];
      if (team && player) {
        // remove player from any team he's in
        game.utils.getTeams().forEach((t) => {
          for (let i = 0; i < t.players.length; i++) {
            if (t.players[i].clientId === data.clientId) {
              t.players.splice(i, 1);
            }
          }
        });
        player.team = data.team;
        team.players.push(player);
        callback(response.ok({
          color: team.color,
          status: 'success',
        }));
      }
    });

    socket.on('PLAYER_READY', (data, callback) => {
      const player = players[data.clientId];
      if (player) {
        player.ready = !player.ready;
        callback(response.ok({
          gameInProgress: game.data.gameInProgress,
          playerReady: player.ready,
          gameId: game.data.gameId,
          status: 'success',
        }));
        game.data.events.playerReady.dispatch(player);
      }
    });
  }
}

export default Game;

// game.canvas.style.transform = 'translate(50%, 50%)';
