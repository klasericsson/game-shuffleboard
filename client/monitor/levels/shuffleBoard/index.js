import Phaser from 'phaser';
// import { center } from '../../../shared/utils/responsive';
import { socket, sendMessage, sendRequestTo } from '../../../shared/socket';
import config from '../../../shared/utils/config';
import CircleSprite from '../../../shared/sprites/CircleSprite';
import PlayerSprite from '../../../shared/sprites/PlayerSprite';
import ScoreZone from '../../sprites/ScoreZone';
import ScoreZoneInner from '../../sprites/ScoreZoneInner';
import { msToTimeString } from '../../../shared/utils/helpers';

const { response } = require('../../../../common/response');

const MAX_PLAYERS = 8;
const MAX_PLAYERS_PER_TEAM = 4;
const SCORE_ZONE_RADIUS = 350;
const MAX_SPRITES = 100;
const MAX_STALL_TIME = 10 * Phaser.Timer.SECOND;

const players = {};
const gameTime = 10 * Phaser.Timer.MINUTE;
const playerTime = 10 * Phaser.Timer.SECOND;

let currentPlayer = null;

let gameEndEvent;

let gameTimer;

const debugEl = document.getElementById('debug');
setInterval(() => {
  let playerStr = '';
  Object.keys(players).forEach(key => {
    playerStr += `${players[key].name} - ${players[key].active}<br>`;
  });
  debugEl.innerHTML = playerStr;
}, 1000);

const calculateQueue = () => {

};

// game rules
// get as close as possible to the center
// the closest player gets points on every game tick
// (maybe give 2nd and 3rd points but on a slower tick)
// if player has the closest puck he can't play
// pulsate ala google maps current location when adding scores

// alt rules
// the zones in a circle
// having a puck inside the zone ticks points to the player
// a player can have several pucks inside zone


// zoom camera out when detecting a new puck entering
// zoom camera on goal when field is idle and no pucks are entering

// ONE AT A TIME

// SUPER ALTERNATIVE MIGHTNOT WORK
// if you knock others inside the field they turn into your colour. Might not work if no of players > 10

// monitor display whole field
// player phone viewport is static and puck can exit world
// calculate exit velocity and direction when puck touches top of screen
// use this velocity direction to spawn the puck on the monitor just under the bottom edge (negative size of sprite)
// goal area pulsates once every N second, thats when points tick to whoever is in the field
// when pulse touches a puck it emits a particle
// alternatively a vertical sweep line (masked outside circle) that hands out points when touched


//** */
// when player (phone) shoots puck, calculate exit velocity and direction when edge of puck touches the top of the screen
// use this velocity direction to spawn the puck on the monitor just under the bottom edge (negative size of sprite)
// white neon line shows the way (maybe use distance indicators?)
//** **/

export default class Shuffleboard extends Phaser.State {

  init() {
    const { game } = this;
    game.data.gameInProgress = true;
    return;
    this.bindEvents();
    
    this.game.stage.backgroundColor = '#222222';

    const one = this.input.keyboard.addKey(Phaser.Keyboard.ONE);
    const two = this.input.keyboard.addKey(Phaser.Keyboard.TWO);
    const three = this.input.keyboard.addKey(Phaser.Keyboard.THREE);
    const four = this.input.keyboard.addKey(Phaser.Keyboard.FOUR);

    one.onDown.add(() => {
      this.camera.scale.set(this.camera.scale.x + 0.2);
    });
    two.onDown.add(() => {
      this.camera.scale.set(this.camera.scale.x - 0.2);
    });
    three.onDown.add(() => {
      this.add.tween(this.camera.scale).to({ x: 1, y: 1 }, 1000, Phaser.Easing.Sinusoidal.Out, true);
    });
    four.onDown.add(() => {
      this.add.tween(this.camera.scale).to({ x: 0.4, y: 0.4 }, 1000, Phaser.Easing.Sinusoidal.Out, true);
    });
  }

  bindEvents() {
    // socket.on('PUCK_MOVE', payload => {
    //   console.log(`received puck move from ${payload.clientId}: x: ${payload.x}, y: ${payload.y}`);
    //   this.puck.body.velocity.x = 0;
    //   this.puck.body.velocity.y = 0;
    //   this.puck.body.x = payload.x;
    //   this.puck.body.y = payload.y;
    //   this.puck.body.damping = 0.35;
    // });

    socket.on('SET_PUCK_VELOCITY', (payload, callback) => {
      console.log(`received puck velocity from ${payload.clientId}`);
      callback(response.ok('Puck velocity received'));
      this.puck.body.velocity.x = payload.x;
      this.puck.body.velocity.y = payload.y;
    });
  }

  preload() {
    this.game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
    this.load.image('backdrop', 'assets/levels/shuffleboard/background.png');
    this.load.image('puck', 'assets/levels/shuffleboard/puck.png');
  }

  setCurrentPlayer(player) {
    this.game.data.currentPlayer = player;
    sendMessage('CURRENT_PLAYER', { currentPlayer: player });
  }

  create() {
    return;
    const game = this.game;
    this.world.setBounds(0, 0, game.width * 1.5, 2000);
    
    // const background = game.add.tileSprite(0, 0, game.world.width, game.world.height, 'backdrop');
    this.background = game.add.tileSprite(0, 0, game.width, game.height, 'backdrop');
    window.background = this.background;
    
    // const backdrop = this.add.sprite(game.world.centerX, game.world.centerY, 'backdrop');
    // backdrop.anchor.setTo(0.5);
    // backdrop.alpha = 0.1;
    // backdrop.scale.setTo(1, 1);


    this.scoreZone = new ScoreZone(game, game.world.centerX, (SCORE_ZONE_RADIUS * 2) + (0.05 * SCORE_ZONE_RADIUS), SCORE_ZONE_RADIUS);
    this.game.add.existing(this.scoreZone);
    // const scoreZoneInner = new ScoreZoneInner(game, game.world.centerX, game.world.centerY);
    // this.game.add.existing(scoreZoneInner);

    game.time.advancedTiming = true;

    gameTimer = this.time.create();
    gameEndEvent = gameTimer.add(gameTime, this.endGame, this);
    gameTimer.start();

    this.physics.startSystem(Phaser.Physics.P2JS);
    this.physics.p2.setImpactEvents(true);
    this.physics.p2.restitution = 0.8;
    const playerCollisionGroup = game.physics.p2.createCollisionGroup();
    const circleCollisionGroup = game.physics.p2.createCollisionGroup();
    this.physics.p2.updateBoundsCollisionGroup();

    // for (let i = 0; i < 300; i++) {
    //   const puck2 = new CircleSprite(this.game, { scale: 0.5, hue: this.rnd.integerInRange(200, 240) });
    //   // const puck2 = new CircleSprite(this.game, { hue: this.rnd.integerInRange(200, 240) });
    //   puck2.body.x = this.rnd.integerInRange(-1000, 1000);
    //   puck2.body.y = this.rnd.integerInRange(-1000, 1000);
    //   this.game.add.existing(puck2);
    //   console.log(puck2);
    //   // const puck2 = this.add.sprite(this.rnd.integerInRange(-1000, 1000), this.rnd.integerInRange(0, 1000), 'puck');
    //   // this.physics.p2.enable([puck2], true);
      
    //   puck2.body.clearShapes();
    //   puck2.data.physicsShape = puck2.body.setCircle(puck2.body.width);
    //   puck2.body.velocity.y = 40.5;
    //   puck2.body.velocity.y = 40.5;
    //   puck2.body.angularVelocity = 1;
    //   puck2.body.setCollisionGroup(circleCollisionGroup);
    //   puck2.body.collides([circleCollisionGroup, playerCollisionGroup]);

    //   puck2.body.onBeginContact.add(this.handleCollision, this);
    // }
    

    this.puckGroup = this.game.add.group();
    this.puckGroup.enableBodyDebug = true;
    this.puckGroup.enableBody = true;
    this.puckGroup.physicsBodyType = Phaser.Physics.P2JS;

    this.puck = new PlayerSprite(this.game, game.world.centerX, game.world.centerY + 800);
    this.game.add.existing(this.puck);
    // this.puck = this.puckGroup.create(this.game.width / 2, this.game.height / 2, 'puck');
    this.puck.body.setCollisionGroup(playerCollisionGroup);
    this.puck.body.collides([circleCollisionGroup]);

    this.game.camera.follow(this.puck, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
    // const test = new CircleSprite(this.game, { lifespan: 10000, hue: 200, enablePhysics: false });
    // test.x = 0;
    // test.y = 0;
    // this.puck.addChild(test);

    // this.puck.body.onBeginContact.add(this.handleCollision, this);

    window.puck = this.puck;

    this.game.camera.x = this.game.world.centerX;
    this.game.camera.y = this.game.world.centerY;

  }

  handleCollision(bodyA, bodyB) {

    if (bodyB.parent && bodyB.parent.sprite.inCamera) {

      const particle = new CircleSprite(this.game, { hue: bodyB.parent.sprite.hue, scale: 1, lifespan: 2500, enablePhysics: false, scaleFactor: 20 });
      // particle.body.x = bodyB.parent.sprite.x;
      // particle.body.y = bodyB.parent.sprite.y;
      particle.x = 0;
      particle.y = 0;
      bodyB.parent.sprite.addChild(particle);
      

      // particle.body.velocity = bodyB.parent.velocity;
      // const particle2 = new CircleSprite(this.game, { hue: bodyB.parent.sprite.hue, lifespan: 4000, enablePhysics: false });
      // particle2.x = bodyB.parent.sprite.x;
      // particle2.y = bodyB.parent.sprite.y;
      // this.game.add.existing(particle2);
    }
  }

  nextPlayer() {

  }

  shutdown() {
       
  }

  endGame() {
    console.log('end');
  }

  update() {
    const speed = 500;
    if (this.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
      this.puck.body.velocity.x = -speed;
    } else if (this.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
      this.puck.body.velocity.x = speed;
    } else if (this.input.keyboard.isDown(Phaser.Keyboard.UP)) {
      this.puck.body.velocity.y = -speed;
    } else if (this.input.keyboard.isDown(Phaser.Keyboard.DOWN)) {
      this.puck.body.velocity.y = speed;
    }

    if (this.input.keyboard.isDown(Phaser.Keyboard.X)) {
      this.puck.body.velocity.x = 0;
      this.puck.body.velocity.y = 0;
    }

    if (this.background) {
      this.background.position.x = this.camera.x;
      this.background.position.y = this.camera.y;
      this.background.tilePosition.x = -this.camera.x;
      this.background.tilePosition.y = -this.camera.y;
    }
    // if (this.game.camera.target && this.puck.body.y < this.scoreZone.y + SCORE_ZONE_RADIUS) {
    //   // this.camera.focusOn(this.puck);
    //   this.camera.unfollow();
    //   this.game.add.tween(this.game.camera).to(
    //     {
    //       x: this.scoreZone.x - (this.camera.width / 2),
    //       y: this.scoreZone.y - (this.camera.height / 2),
    //     }, 750, Phaser.Easing.Quadratic.InOut, true
    //   );
    // }

    if (this.game.camera.target && this.puck.body.y < this.scoreZone.y) {
      // this.setCurrentPlayerI('HORAN');
    }

    // if (!this.game.camera.target && this.puck.body.y > this.scoreZone.y + SCORE_ZONE_RADIUS) {
    //   // this.camera.focusOn(this.puck);
    //   this.game.camera.follow(this.puck, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
    // }
    // console.log(this.camera);
    // this.camera.focusOnXY(this.puck.body.x, this.puck.body.y);
  }

  formatTime(s) {
    // Convert seconds (s) to a nicely formatted and padded time string
    const minutes = `0${Math.floor(s / 60)}`;
    const seconds = `0${s - (minutes * 60)}`;
    // const hundreds = `0${s - (minutes * 60)}`;
    return `${minutes.substr(-2)}:${seconds.substr(-2)}`;
  }

  render() {
    // this.game.debug.cameraInfo(this.game.camera, 32, 32, '#00ff00');
    // this.game.debug.text(`scale: ${this.camera.scale.x}`, 40, 160, '#ff00ff');

    // this.game.debug.text(this.math.distance(this.scoreZone.x, this.scoreZone.y, this.puck.body.x, this.puck.body.y), 400, 20);

    // this.game.debug.text(`sprites: ${this.world.children.length}`, 40, 200, '#ffffff');
    // // this.game.debug.text(`worldX: ${this.camera.scale.x}`, 40, 200, '#ff0000');

    // this.game.debug.text(this.game.time.fps, 40, 300, '#ffff00');

    // if (gameTimer.running) {
    //   // this.game.debug.text(msToTimeString(gameEndEvent.delay - gameTimer.ms));
    //   this.game.debug.text(msToTimeString(gameEndEvent.delay - gameTimer.ms), 2, 14, '#ff0');
    //   // this.game.debug.text(this.formatTime(Math.round((gameEndEvent.delay - gameTimer.ms) / 1000)), 2, 14, '#ff0');
    // }
  }

}
