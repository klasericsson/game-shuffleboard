/* globals __DEV__ */
import Phaser from 'phaser';
import sausage from '../../../shared/levels/sausage/objects/Sausage';
import { center } from '../../../shared/utils/responsive';
import { socket } from '../../../shared/socket';

const { response } = require('../../../../common/response');

export default class extends Phaser.State {

  init() {
    console.log('sausage level init');
    this.bindEvents();
  }

  bindEvents() {
    socket.on('SLICE_SAUSAGE', (payload, callback) => {
      console.log(`received slice message from ${payload.clientId}: xPercent: ${payload.xPercent}`);
      callback(response.ok('Slice received'));
      this.sausage.slice(payload.xPercent * this.sausage.sprite.width);
    });
  }

  preload() {
    for (const asset of sausage.assets) {
      this.load.image(asset.key, asset.path);
    }
  }

  create() {
    const { game } = this;
    game.physics.startSystem(Phaser.Physics.Arcade);
    this.sausage = sausage.create(game, 0, 0, game.world.width * 0.9);
    center(this.sausage.sprite, this.world);
  }

  update() {

  }

  render() {

  }
}
