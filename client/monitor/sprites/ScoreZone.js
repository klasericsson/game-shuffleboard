/* global Phaser, PIXI */

class ScoreZone extends Phaser.Sprite {
  constructor(game, x, y, radius = 256) {
    super(game, x, y);
    // this.blendMode = PIXI.blendModes.ADD;
    this.anchor.setTo(0.5, 0.5);

    this.data = {
      radius,
      textureSize: 1024,
      outerLineWidth: 0.1 * radius * 2,
      innerLineWidth: 0.1 * radius * 2,
      innerSize: 0.3 * radius * 2,
      leadCircleSize: 0.75 * radius * 2,
      leadCircleLineWidth: 0.2 * radius * 2,
    };

    this.data.parts = {
      outerCircle: this.createOuterCircle(),
      innerCircle: this.createInnerCircle(),
      createLeadCircle: this.createLeadCircle(),
    };

    const bmd = this.game.make.bitmapData(radius * 2, radius * 2);
    const { context } = bmd;
    context.beginPath();
    context.fillStyle = 'rgba(0, 0, 0, 1)';
    // context.rect(0, 0, textureSize, textureSize);
    context.arc(bmd.width / 2, bmd.height / 2, radius, 0, 2 * Math.PI, false);
    context.fill();

    this.loadTexture(bmd);

    // const alphaTween = game.add.tween(test2).to({ alpha: 0.5 }, 3000, Phaser.Easing.EaseOut, true).loop(true);
    // alphaTween.yoyo(true, 0); // wait 1000ms
  }

  createOuterCircle() {
    const { radius, textureSize, outerLineWidth } = this.data;
    const bmd = this.game.make.bitmapData(textureSize, textureSize);
    const { context } = bmd;
    context.beginPath();
    context.arc(textureSize / 2, textureSize / 2, radius - (outerLineWidth / 2), 0, 2 * Math.PI, false);
    context.shadowBlur = 16;
    context.shadowColor = '#00ccff';
    context.lineWidth = outerLineWidth;
    context.strokeStyle = 'hsla(192, 100%, 50%, 0.5)';
    context.stroke();
    return this.addChild(this.game.make.sprite(-textureSize / 2, -textureSize / 2, bmd));
  }

  createInnerCircle() {
    const { innerSize, textureSize, innerLineWidth } = this.data;
    const bmd = this.game.make.bitmapData(textureSize, textureSize);
    const { context } = bmd;
    context.beginPath();
    context.arc(textureSize / 2, textureSize / 2, (innerSize / 2) - (innerLineWidth / 2), 0, 2 * Math.PI, false);
    context.shadowBlur = 16;
    context.shadowColor = '#00ccff';
    context.lineWidth = innerLineWidth;
    context.strokeStyle = 'hsla(192, 100%, 50%, 0.5)';
    context.stroke();
    return this.addChild(this.game.make.sprite(-textureSize / 2, -textureSize / 2, bmd));
  }

  createLeadCircle() {
    const { leadCircleSize, textureSize, leadCircleLineWidth } = this.data;
    const bmd = this.game.make.bitmapData(textureSize, textureSize);
    const { context } = bmd;
    context.beginPath();
    context.arc(textureSize / 2, textureSize / 2, (leadCircleSize / 2) - (leadCircleLineWidth / 2), 0, 2 * Math.PI, false);
    context.shadowBlur = 16;
    context.shadowColor = '#ffffff';
    context.lineWidth = leadCircleLineWidth;
    context.strokeStyle = 'rgba(255, 255, 255, 0.4)';
    // context.strokeStyle = 'rgba(0, 255, 0, 0.4)';
    context.stroke();
    return this.addChild(this.game.make.sprite(-textureSize / 2, -textureSize / 2, bmd));
  }

  fade() {

  }

  kill() {
    super.kill();
  }
}

export default ScoreZone;
