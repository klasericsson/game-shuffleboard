/* global Phaser, PIXI */

const textureSize = 1024;
const circleSize = 340;

class ScoreZoneInner extends Phaser.Sprite {
  constructor(game, x, y) {
    super(game, x, y);
    this.blendMode = PIXI.blendModes.ADD;
    this.anchor.setTo(0.5, 0.5);
    const bmd = game.make.bitmapData(textureSize, textureSize);
    const { context } = bmd;
    context.beginPath();
    context.arc(textureSize / 2, textureSize / 2, (circleSize / 2), 0, 2 * Math.PI, false);
    context.fillStyle = 'rgba(255, 255, 255, 0.3)';
    context.shadowBlur = 12;
    context.shadowColor = '#ffffff';
    context.lineWidth = 0;
    context.fill();
    // const test2 = game.make.sprite(0, 0, bmd);
    // this.addChild(test2);
    this.loadTexture(bmd);
  }
}

export default ScoreZoneInner;
