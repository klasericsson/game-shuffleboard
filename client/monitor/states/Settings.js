import Phaser from 'phaser';
import { renameClient, sendRequest } from '../../shared/socket';
import config from '../../shared/utils/config';

const MONITOR_NAME = 'monitor';

export default class extends Phaser.State {
  init() {
    // connect();
  }

  preload() {
  }
  // prompt('set name please')
  create() {
    // hardcode monitorClientId for dev purposes
    renameClient({ newId: MONITOR_NAME })
    .then(reply => {
      // ok to proceed to lobby
      this.state.start('ShuffleboardLevel');
      console.log(reply);
    })
    .catch((error) => {
      console.log('Please choose a different game name');
      console.error(error);
    });
  }

  update() {

  }

}
