import Phaser from 'phaser';
import shortid from 'shortid';
import { socket, sendMessage, sendRequest } from '../../shared/socket';
import config from '../../shared/utils/config';
import { defaultStyle } from '../../shared/utils/font';

const { response } = require('../../../common/response');

const COUNTDOWN_TIME = 5 * Phaser.Timer.SECOND;
const playerLists = [];

export default class extends Phaser.State {
  init() {
    const { data } = this.game;
    data.gameId = shortid.generate();
    Object.keys(data.players).forEach((key) => { delete data.players[key]; });
    Object.keys(data.teams).forEach((key) => {
      data.teams[key].players = [];
      data.teams[key].score = 0;
      data.teams[key].turnIndex = 0;
    });

    // data.teams['red'].players.push({ name: 'Olle', team: 'red', ready: true });
    // data.teams['red'].players.push({ name: 'Lars', team: 'red', ready: false });
    // data.teams['yellow'].players.push({ name: 'Banan', team: 'red', ready: true });
  }

  preload() {

  }

  create() {
    this.startGame();

    console.log('Lobby state. Waiting for players');
    console.log(`Game name: ${config.clientId}`);
    const { game } = this;
    const { data } = game;

    data.events.playerReady.add(this.playerReady, this);

    this.startTimer = {
      game,
      running: false,
      ms: COUNTDOWN_TIME,
      startTime: 0,
      currentTime: 0,
      stop: function() {
        this.running = false;
      },
      start: function() {
        this.startTime = this.game.time.now + this.ms;
        this.running = true;
      },
      tick: function() {
        this.currentTime = this.startTime - this.game.time.now;
        if (this.currentTime <= 0) {
          this.currentTime = 0;
        }
      },
    };

    this.statusText = this.add.text(game.world.centerX, this.game.height * 0.9, 'Go to http://joingame.online to join', defaultStyle);
    this.statusText.anchor.set(0.5, 0.5);

    const countdownStyle = Object.assign({}, defaultStyle, { font: '128px Segoe UI', fill: '#ffffff' });
    this.countDownText = this.add.text(game.world.centerX, game.world.centerY, '10', countdownStyle);
    this.countDownText.anchor.set(0.5, 0.5);

    this.playerLists = [];
    const numberOfTeams = Object.keys(data.teams).length;
    const paddingLeft = 0.04 * this.game.width;
    const marginWidth = (this.game.width / numberOfTeams) - (paddingLeft / 2);
    Object.keys(data.teams).forEach((key, i) => {
      const team = data.teams[key];
      let xPos = paddingLeft + (marginWidth * i);
      const yPos = this.game.height * 0.1;
      const headingStyle = Object.assign({}, defaultStyle, { font: '36px Segoe Black', fill: team.color, align: 'left' });
      const listStyle = Object.assign({}, defaultStyle, { font: '22px Segoe', fill: '#ffffff', align: 'left' });
      if (i === numberOfTeams - 1) {
        xPos += paddingLeft / 2;
      }

      this.add.text(xPos, yPos, `Team ${key}`.toUpperCase(), headingStyle);
      playerLists.push({
        team,
        text: this.game.add.text(xPos + 2, yPos + 50, '', listStyle),
      });
    });

    this.game.add.tween(this.statusText.scale).to(
      { x: 0.9, y: 0.9 },
      300, Phaser.Easing.Sinusoidal.InOut, true, 500).loop(true).yoyo(true);
  }

  playerReady(player) {
    const { game } = this;
    const allPlayers = game.utils.getPlayers();
    if (allPlayers.length > 0 && game.utils.getPlayers().filter(p => !p.ready).length === 0) {
      this.startCountDown();
    } else {
      this.startTimer.stop();
    }
  }

  startCountDown() {
    console.log('starting countdown');
    this.startTimer.start();
  }

  startGame() {
    sendMessage('GAME_START', { gameId: this.game.data.gameId });
    this.state.start('ShuffleboardLevel');
  }

  shutdown() {
    console.log('removing event listenre!!!');
    this.game.data.events.playerReady.removeAll();
  }

  update() {
    // this.background.tilePosition.y += 0.5;
    // this.background.tilePosition.x -= 0.5;
    for (let i = 0; i < playerLists.length; i++) {
      playerLists[i].text.setText(
        playerLists[i].team.players.map((player) => `${player.name} - ${player.ready ? 'READY' : 'WAITING'}`).join('\n')
      );
    }

    if (this.startTimer.running) {
      this.startTimer.tick();
      this.countDownText.setText(Math.ceil(this.startTimer.currentTime / 1000));
      if (this.startTimer.currentTime <= 0) {
        this.startGame();
        this.startTimer.stop();
      }
    } else {
      this.countDownText.setText('');
    }
  }

}
