import Phaser from 'phaser';
import WebFont from 'webfontloader';

export default class extends Phaser.State {
  init() {
    this.stage.backgroundColor = '#000';
    this.fontsReady = false;
    this.fontsLoaded = this.fontsLoaded.bind(this);
  }

  create() {
    this.state.start('Lobby');
  }

  preload() {
    // WebFont.load({
    //   google: {
    //     families: ['Nunito'],
    //   },
    //   active: this.fontsLoaded,
    // });
  }

  render() {
    // if (!this.fontsReady) {
      
    // }
  }

  fontsLoaded() {
    // this.fontsReady = true;
  }

}
