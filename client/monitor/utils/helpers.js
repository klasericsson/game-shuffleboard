import { sendMessage, onReceiveMessage, removeMessageListener } from '../../shared/socket';
import { game } from '../main';

export const setMonitorSetting = (key, value) =>
new Promise((resolve, reject) => {
  const messageId = sendMessage('SET_MONITOR_SETTING', {
    key,
    value,
  });
  const cleanup = () => {
    console.log('cleanup!');
    removeMessageListener('MONITOR_SETTING_SAVED', savedCallback);
    removeMessageListener('ERROR', errorCallback);
  };
  const savedCallback = () => {
    game.x[key] = value;
    cleanup();
    return resolve(game.x);
  };
  const errorCallback = err => {
    if (err.messageId === messageId) {
      cleanup();
      return reject(err);
    }
    return true;
  };
  onReceiveMessage('MONITOR_SETTING_SAVED', savedCallback);
  return onReceiveMessage('ERROR', errorCallback);
});
