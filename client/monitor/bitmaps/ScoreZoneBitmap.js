class ScoreZoneBitmap extends Phaser.BitmapData {
  constructor(game, key, width, height) {
    super(game, key, width, height);
    const radius = width / 2;
    const colour = '2, 100%, 50%';

    // fill gradient
    const grd = this.context.createRadialGradient(radius, radius, 1, radius, radius, radius);
    const hslaColour = 'hsla(' + colour;
    grd.addColorStop(0, hslaColour + ',0)');
    grd.addColorStop(0.5, hslaColour + ',0.3)');
    grd.addColorStop(1, 'hsla(0,0%,0%,0)');

    this.context.shadowBlur = 10;
    this.context.shadowColor = "red";

    this.context.fillStyle = grd;
    this.context.arc(radius, radius, radius, 0, Math.PI * 2);
    this.context.fill();
  }
}

export default ScoreZoneBitmap;
