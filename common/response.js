const response = {
  ok: (payload) => JSON.stringify({
    error: null,
    content: payload,
  }),
  error: (error) => JSON.stringify({ error }),
};

if (module) {
  module.exports = { response };
}
